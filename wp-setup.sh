#!/bin/bash

set -e

function usage {
  echo "USAGE: $1 [-D OPTION] [-p OPTION] -h"
  echo "
    OPTIONS: 
    -D Directory to downoad wp. Default $HOME/$USER/Downloads
    -p path to wp setup
    -h prints this help message
  "
  exit 1;
}

if [[ $EUID -ne 0 ]]; then
  echo >&2 "RUN AS ROOT"
fi

if [[ $# -eq 0 ]]; then
  echo "This script requires arguments"
  usage
fi

dir="$HOME/$USER/Downloads"
path=""
while getopts D:p:h flag; do
  case $flag in
    D) dir=$OPTARG;;
    p) path=$OPTARG;;
    h) usage;;
    *) echo "Invalid flag: $flag"; usage;;
  esac
done 

shift $(($OPTIND - 1))

if [[ $path = "" ]]; then
  echo >&2 "You have not provided path to Wordpress setup"
fi

if ! [ -d $dir ]; then
  mkdir $dir
  cd $dir
  echo "Downloading wordpress"
  wget https://wordpress.org/latest.tar.gz
  tar -xvzf $dir/latest.tar.gz
else
  cd $dir 
  echo "Downloading wordpress"
  wget https://wordpress.org/latest.tar.gz
  tar -xvzf $dir/latest.tar.gz
fi
if ! [ -d $path ]; then
  echo >&2 "Directory $path doesn't exist"
else
  echo "Copying Wordpress installation files to $path"
  cp -R $dir/wordpress/* $path
  chown -R www-data.www-data $path
fi

echo "finished"
